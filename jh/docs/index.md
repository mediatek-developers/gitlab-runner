---
comments: false
stage: Verify
group: Runner
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# GitLab Runner

<!--
GitLab Runner is an application that works with GitLab CI/CD to run jobs in a pipeline.

You can choose to [**install**](install/index.md) the GitLab Runner application
on infrastructure that you own or manage. If you do, you should install
GitLab Runner on a machine that's separate from the one that hosts the GitLab instance
for security and performance reasons. When you use separate machines, you can have
different operating systems and tools, like Kubernetes or Docker, on each.

GitLab Runner is open-source and written in [Go](https://golang.org). It can be run
as a single binary; no language-specific requirements are needed.

You can install GitLab Runner on several different supported operating systems.
Other operating systems may also work, as long as you can compile a Go
binary on them.

GitLab Runner can also run inside a Docker container or be deployed into a Kubernetes cluster.

View some [best practices](best_practice/index.md) for how to use and administer GitLab Runner.
-->

GitLab Runner 是在流水线中运行作业的应用，与 GitLab CI/CD 配合运作。

您可以选择在您拥有或管理的基础设施上安装 GitLab Runner 应用程序。如果您这样做，考虑到安全和性能原因，您应该在未托管极狐GitLab 实例的机器上安装 GitLab Runner。当您使用不同的机器时，您可以在每个机器上使用不同的操作系统和工具，例如 Kubernetes 或 Docker。

GitLab Runner 是开源的，使用 Go 语言编写。它可以作为单个二进制文件运行；没有特定的语言要求。

您可以在几种不同的官方支持操作系统上安装 GitLab Runner。其它操作系统也可能适用，只要您可以在上面编译 Go 二进制文件。

GitLab Runner 也可以在 Docker 容器内运行或部署到 Kubernetes 集群中。

查看有关如何使用和管理 GitLab Runner 的[最佳实践](best_practice/index.md)。

## GitLab Runner 版本

<!--
For compatibility reasons, the GitLab Runner [major.minor](https://en.wikipedia.org/wiki/Software_versioning) version should stay in sync with the GitLab major and minor version. Older runners may still work
with newer GitLab versions, and vice versa. However, features may be not available or work properly
if a version difference exists.

Backward compatibility is guaranteed between minor version updates. However, sometimes minor
version updates of GitLab can introduce new features that require GitLab Runner to be on the same minor
version.
-->

由于兼容性原因，GitLab Runner 的 `主版本.小版本` 应和极狐GitLab 的 `主版本.小版本` 保持同步。老版本的 GitLab Runner 可能适用于新版本的极狐 GitLab，反之亦然。但是如果存在版本差异，功能特性可能不可用或无法正常工作。

在小版本更新之间保证向后兼容。但是有时小版本更新会引入要求 GitLab Runner 处于相同版本的新特性功能。

## Runner 注册

<!--
After you install the application, you [**register**](register/index.md)
individual runners. Runners are the agents that run the CI/CD jobs that come from GitLab.

When you register a runner, you are setting up communication between your
GitLab instance and the machine where GitLab Runner is installed.

Runners usually process jobs on the same machine where you installed GitLab Runner.
However, you can also have a runner process jobs in a container,
in a Kubernetes cluster, or in auto-scaled instances in the cloud.
-->

在您安装应用后，您注册了个人的 Runner。Runners 是运行来自极狐 GitLab 的 CI/CD 作业的代理。

当您注册 Runner 时，您在极狐GitLab 实例和安装了 GitLab Runner 的机器之间建立通信。

Runners 通常在您安装了 GitLab Runner 的机器上处理作业。除此之外，您也可以在容器中，在 Kubernetes 集群中，或在云端的弹性主机上运行 Runner 处理作业。

### 执行器

<!--
When you register a runner, you must choose an executor.

An [**executor**](executors/README.md) determines the environment each job runs in.

For example:

- If you want your CI/CD job to run PowerShell commands, you might install GitLab
  Runner on a Windows server and then register a runner that uses the shell executor.
- If you want your CI/CD job to run commands in a custom Docker container,
  you might install GitLab Runner on a Linux server and register a runner that uses
  the Docker executor.

These are only a few of the possible configurations. You can install GitLab Runner
on a virtual machine and have it use another virtual machine as an executor.

When you install GitLab Runner in a Docker container and choose the
[Docker executor](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html)
to run your jobs, it's sometimes referred to as a "Docker-in-Docker" configuration.
-->

当您注册一个 Runner 时，您必须选择一个执行器。

[**执行器**](executors/README.md)决定每个作业运行的环境。

例如：

* 如果您想要 CI/CD 作业运行 Powershell 命令，您可以在 Windows 服务器上安装 GitLab Runner，然后注册使用 shell 执行器的 Runner。
* 如果您想要 CI/CD 作业在自定义 Docker 容器中运行命令，您可以在 Linux 服务器上安装 GitLab Runner，然后注册使用 Docker 执行器的 Runner。

以上只是几种可能的配置。您可以在虚拟机上安装 GitLab Runner，并让它使用另一个虚拟机作为执行器。

当您在 Docker 容器中安装 GitLab Runner，并选择 [Docker 执行器](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html)运行作业时，有时被称为 "Docker-in-Docker" 配置。

### 谁有权限在 UI 中访问 Runner

<!--
Before you register a runner, you should determine if everyone in GitLab
should have access to it, or if you want to limit it to a specific GitLab group or project.

There are three types of runners, based on who you want to have access:

- [Shared runners](https://docs.gitlab.com/ee/ci/runners/README.html#shared-runners) are for use by all projects
- [Group runners](https://docs.gitlab.com/ee/ci/runners/README.html#group-runners) are for all projects and subgroups in a group
- [Specific runners](https://docs.gitlab.com/ee/ci/runners/README.html#specific-runners) are for individual projects

When you register a runner, you specify a token for the GitLab instance, group, or project.
This is how the runner knows which projects it's available for.
-->

在您注册 Runner 之前，您应该确定是否每个人应该有权访问。或者您想要限制其适用于指定的群组或项目。

根据您想要允许访问的用户，分为三种类型的 Runner：

* [Shared runners](https://docs.gitlab.cn/ee/ci/runners/README.html#shared-runners) 允许所有项目使用。
* [Group runners](https://docs.gitlab.cn/ee/ci/runners/README.html#group-runners) 允许指定群组下的所有项目和子组使用。
* [Specific runners](https://docs.gitlab.cn/ee/ci/runners/README.html#specific-runners) 允许指定的个人项目使用。

注册 Runner 时，您可以为极狐GitLab 实例、群组或项目指定 token。这就是 Runner 知道它适用于哪些项目的方式。

### 标签

<!--
When you register a runner, you can add [**tags**](https://docs.gitlab.com/ee/ci/yaml/README.html#tags) to it.

When a CI/CD job runs, it knows which runner to use by looking at the assigned tags.

For example, if a runner has the `ruby` tag, you would add this code to
your project's `.gitlab-ci.yml` file:

```yaml
job:
  tags:
    - ruby
```

When the job runs, it uses the runner with the `ruby` tag.
-->

当您注册 Runner 时，您可以向其添加[**标签**](https://docs.gitlab.com/ee/ci/yaml/README.html#tags)。

当运行 CI/CD 作业时，通过分配的标签确定要使用的 Runner。

例如，如果一个 Runner 有 `ruby` 标签，您可以将此代码添加到您项目中的 `.gitlab-ci.yml` 文件：

```yaml
job:
  tags:
    - ruby
```

当运行作业时，使用带有 `ruby` 标签的 Runner。

## 配置 Runners

<!--
You can [**configure**](configuration/advanced-configuration.md)
the runner by editing the `config.toml` file. This is a file that is installed during the runner installation process.

In this file you can edit settings for a specific runner, or for all runners.

You can specify settings like logging and cache. You can set concurrency,
memory, CPU limits, and more.
-->

您可以通过编辑 `config.toml` 文件来[**配置**](configuration/advanced-configuration.md) Runner。该文件在 Runner 安装过程中同时安装。

在该文件中，您可以为指定 Runner 或全部 Runner 编辑设置。

您可以指定日志记录和缓存等设置。您可以设置并发、内存、CPU 限制等。

## 监控 Runners

<!--
You can use Prometheus to [**monitor**](monitoring/README.md) your runners.
You can view things like the number of currently-running jobs and how
much CPU your runners are using.
-->

您可以使用 Prometheus [**监控**](monitoring/README.md)您的 Runners。
您可以查看例如正在运行的作业数量，和正在使用的 CPU 资源等指标。

## 使用 Runner 运行作业

<!--
After a runner is configured and available for your project, your
[CI/CD](https://docs.gitlab.com/ee/ci/README.html) jobs can use the runner.

Specify the name of the runner or its tags in your
[`.gitlab-ci.yml`](https://docs.gitlab.com/ee/ci/yaml/README.html) file.
Then, when you commit to your repository, the pipeline runs, and
the runner's executor processes the commands.
-->

在为您的项目配置 Runner 并使其可用后，您的 [CI/CD](https://docs.gitlab.cn/ee/ci/README.html) 作业可以使用 Runner。

在 [`.gitlab-ci.yml`](https://docs.gitlab.com/ee/ci/yaml/README.html) 中指定 Runner 的名称或标签。然后，当您提交到仓库后，流水线运行，Runner 的执行器执行命令。

<!--
## Runners on GitLab.com

If you use GitLab.com, GitLab [manages runners for you](https://docs.gitlab.com/ee/user/gitlab_com/index.html#shared-runners).
These runners are enabled for all projects, though
[you can disable them](https://docs.gitlab.com/ee/ci/runners/README.html#disable-shared-runners).

If you don't want to use runners managed by GitLab, you can install GitLab Runner and
register your own runners on GitLab.com.
-->

## 特性

<!--
GitLab Runner has the following features.

- Run multiple jobs concurrently.
- Use multiple tokens with multiple servers (even per-project).
- Limit the number of concurrent jobs per-token.
- Jobs can be run:
  - Locally.
  - Using Docker containers.
  - Using Docker containers and executing job over SSH.
  - Using Docker containers with autoscaling on different clouds and virtualization hypervisors.
  - Connecting to a remote SSH server.
- Is written in Go and distributed as single binary without any other requirements.
- Supports Bash, PowerShell Core, and Windows PowerShell.
- Works on GNU/Linux, macOS, and Windows (pretty much anywhere you can run Docker).
- Allows customization of the job running environment.
- Automatic configuration reload without restart.
- Easy to use setup with support for Docker, Docker-SSH, Parallels, or SSH running environments.
- Enables caching of Docker containers.
- Easy installation as a service for GNU/Linux, macOS, and Windows.
- Embedded Prometheus metrics HTTP server.
- Referee workers to monitor and pass Prometheus metrics and other job-specific data to GitLab.
-->

GitLab Runner 具有以下特性。

* 并行运行多个作业。
* 在多个服务器上使用多个 token（甚至每个项目）。
* 限制每个 token 的并发作业书。
* 作业可以在以下环境运行作业：
	* 本地。
	* 使用 Docker 容器。
	* 使用 Docker 容器并通过 SSH 执行作业。
	* 在不同的云和虚拟化平台上使用弹性 Docker 容器。
	* 连接到远程 SSH 服务器。
* 使用 Go 语言编写并作为单个二进制文件分发，没有任何其它需求。
* 支持 Bash、PowerShell Core 和 Windows PowerShell。
* 适用于 GNU/Linux、macOS 和 Windows（可以运行 Docker 的几乎任何环境）。
* 允许自定义作业运行环境。
* 无需重启即可自动重新加载配置。
* 易于使用的设置，支持 Docker、Docker-SSH、Parallels 或 SSH 运行环境。
* 启用缓存 Docker 容器。
* 轻松安装为 GNU/Linux、macOS 和 Windows 的服务。
* 嵌入式 Prometheus 指标 HTTP 服务器。
* 支持被监控，并将 Prometheus 指标和其他特定作业的数据传递给极狐GitLab。

## Runner 执行流程

<!--
This diagram shows how runners are registered and how jobs are requested and handled. It also shows which actions use [registration, authentication](https://docs.gitlab.com/ee/api/runners.html#registration-and-authentication-tokens), and [job tokens](https://docs.gitlab.com/ee/user/project/new_ci_build_permissions_model.html#job-token).
-->

下图展示 Runner 是如何注册的，以及如何请求和处理作业。同时还展示哪些操作使用 [注册，认证](https://docs.gitlab.cn/ee/api/runners.html#registration-and-authentication-tokens), 以及[作业 tokens](https://docs.gitlab.cn/ee/user/project/new_ci_build_permissions_model.html#job-token)。

```mermaid
sequenceDiagram
    participant GitLab
    participant GitLabRunner
    participant Executor

    opt registration
      GitLabRunner ->>+ GitLab: POST /api/v4/runners with registration_token
      GitLab -->>- GitLabRunner: Registered with runner_token 
    end

    loop job requesting and handling
      GitLabRunner ->>+ GitLab: POST /api/v4/jobs/request with runner_token
      GitLab -->>+ GitLabRunner: job payload with job_token
      GitLabRunner ->>+ Executor: Job payload
      Executor ->>+ GitLab: clone sources with job_token
      Executor ->>+ GitLab: download artifacts with job_token
      Executor -->>- GitLabRunner: return job output and status
      GitLabRunner -->>- GitLab: updating job output and status with job_token
    end
```

<!--
## Troubleshooting

Learn how to [troubleshoot](faq/README.md) common issues.

## Contributing

Contributions are welcome. See [`CONTRIBUTING.md`](https://gitlab.com/gitlab-org/gitlab-runner/blob/master/CONTRIBUTING.md)
and the [development documentation](development/README.md) for details.

If you're a reviewer of GitLab Runner project, take a moment to read the
[Reviewing GitLab Runner](development/reviewing-gitlab-runner.md) document.

You can also review [the release process for the GitLab Runner project](https://gitlab.com/gitlab-org/gitlab-runner/blob/master/PROCESS.md).

## Changelog

See the [CHANGELOG](https://gitlab.com/gitlab-org/gitlab-runner/blob/master/CHANGELOG.md) to view recent changes.

## License

This code is distributed under the MIT license. View the [LICENSE](https://gitlab.com/gitlab-org/gitlab-runner/blob/master/LICENSE) file.
-->
